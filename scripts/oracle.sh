#!/bin/bash
# Install important packages
aptitude -y install alien libaio1 unixodbc bc ksh

# Create temporary swap partition for Oracle installation
dd if=/dev/zero of=/tmp/swap bs=1M count=4096
chmod 600 /tmp/swap
mkswap /tmp/swap
swapon /tmp/swap
echo "================================================="
echo  "New swap size:"
grep SwapTotal /proc/meminfo

# Create temporary tmpfs with 1,5GB for Oracle installation
echo -e "\ntmpfs           /dev/shm        tmpfs   defaults,size=1512M 0    0" >> /etc/fstab
mount -o remount /dev/shm
echo "================================================="
echo "New tmpfs size:"
df -kh /dev/shm/

# Create chkonfig (Hack 1)
cat > /sbin/chkconfig <<"DLM"
# Oracle 11gR2 XE installer chkconfig hack for Debian by Dude
file=/etc/init.d/oracle-xe
if [[ ! `tail -n1 $file | grep INIT` ]]; then
   echo >> $file
   echo '### BEGIN INIT INFO' >> $file
   echo '# Provides:             OracleXE' >> $file
   echo '# Required-Start:       $remote_fs $syslog' >> $file
   echo '# Required-Stop:        $remote_fs $syslog' >> $file
   echo '# Default-Start:        2 3 4 5' >> $file
   echo '# Default-Stop:         0 1 6' >> $file
   echo '# Short-Description:    Oracle 11g Express Edition' >> $file
   echo '### END INIT INFO' >> $file
fi
update-rc.d oracle-xe defaults 80 01
DLM
chmod 755 /sbin/chkconfig


# Create Debian Appliance Builder and Oracle installation
echo "================================================="
echo "Create oracle-xe_11.2.0-2_amd64.deb..."
cd /root
alien --d --scripts oracle-xe-11.2.0-1.0.x86_64.rpm
dpkg -i oracle-xe_11.2.0-2_amd64.deb
echo "================================================="
echo "Oracle XE package was installed"

# Hack 2 - replace /dev/shm in binary with /run/shm for using in Ubuntu
cd /u01/app/oracle/product/11.2.0/xe/bin && \
  cp oracle oracle.bak && \
  sed 's|/dev/shm|/run/shm|g' oracle.bak >/tmp/oracle && \
  cp /tmp/oracle . && \
  chown oracle:dba oracle && \
  chmod 6751 oracle

echo "================================================="
echo "Hack 2 was created"

# Hack 3 for elimination of the message with "cannot touch `/var/lock/subsys/listener'"
sed -i '33 s/^/\nmkdir -p \/var\/lock\/subsys\n/' /etc/init.d/oracle-xe

# Oracle XE Configure
/etc/init.d/oracle-xe configure

# Clean
rm /sbin/chkconfig
rm /tmp/oracle

echo "================================================="
echo "Complete!"
